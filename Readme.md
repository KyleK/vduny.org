# New VDUNY.org website #

The new VDUNY.org website will be developed in the open as a git repository and will follow the [Gitflow workflow](https://www.atlassian.com/git/workflows#!workflow-gitflow).

The technologies that have been chosen to use for the new site currently include:

- Asp.net w/ C#
	- MVC 5
	- Web API 2
- Entity Framework 6 using Code First w/ DbContext
- Asp.net Identity
	- Local account
	- Google integration
	- *Facebook integration?*
	-  *Microsoft Account integration?*
- Twitter Bootstrap
	- Bootswatch or custom theme
- Knockout JS (client-side MVVM)
	- Knockout-postbox (cross-model pub/sub)
- Modernizr
- Moment.js
- html5shiv (for legacy browser support)
- Respond.js (responsive design for legacy browser support) 